// 1вариант если нажимать мышкой

// document.addEventListener("click", function (e) {

//   idBtn = e.target.id;
//   btnColor = e.target.className;

//   if (btnColor === "btn") {
//     idBtn = document.getElementById(idBtn);

//     if (idBtn .style.background == "blue") {
//         idBtn.style.background = "#000000";
//     } else {

//       const buttons = document.querySelectorAll(".btn");

//       buttons.forEach(button => {
//         button.setAttribute("style", "background:#000000");
//       })

//       idBtn.style.background = "blue";

//     }
//   }
// });

// 2й вариант если нажимать кнопки на клавиатуре


const btns = document.querySelectorAll(".btn");

function checkBtn(key) {
  if(key !== 'Enter') {
    key = key.toUpperCase();
  }
  
  btns.forEach((item) => {
    item.classList.remove('active')
    if (item.textContent === key) {
      item.classList.add('active')
    }
  });
}

document.addEventListener("keydown", (e) => {
  checkBtn(e.key);
});


